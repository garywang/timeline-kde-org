# SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
#
# SPDX-LicenseIdentifier: LGPL-2.0-or-later

import xml.etree.ElementTree as ET
import argparse
import polib
import os
import shutil
import subprocess
import gettext
import json

def find_lines(ids):
    """Find lines in index.html, that contain these IDs
    Parameters:
      ids: a dict_keys gotten from json
    Returns:
      lines: a dict of list of tuples:
      - each key is an ID
      - each value is a list of tuples, each tuple is a pair of file path and line number
    """
    lines = {}
    html_path = 'websites/timeline-kde-org/index.html'
    with open('index.html') as f:
        content = f.readlines()
        for key in ids:
            str_id = 'id="' + key + '"'
            line_nums = [(html_path, x+1) for x in range(len(content)) if str_id in content[x]]
            lines[key] = line_nums
    return lines
    

def extract(args):
    """
    First parameter will be the path of the pot file we have to create
    """
    pot_file = args.pot
    pot = polib.POFile(check_for_duplicates=True)
    pot.metadata = {
        'Project-Id-Version': '1.0',
        'Report-Msgid-Bugs-To': 'kde-www@kde.org',
        'Last-Translator': 'you <you@example.com>',
        'Language-Team': 'English <yourteam@example.com>',
        'MIME-Version': '1.0',
        'Content-Type': 'text/plain; charset=utf-8',
        'Content-Transfer-Encoding': '8bit',
    }
    with open('js/languages.json', 'r') as json_file:
        data = json.load(json_file)
        lines = find_lines(data.keys())
        for item in data.items():
            entry = polib.POEntry(
                msgid=item[1]['en'],
                msgstr=u'',
                msgctxt='id="' + item[0] + '"',
                occurrences=lines[item[0]]
            )
            try:
                pot.append(entry)
            except:
                pass
        pot.save(pot_file)


def xml2json(args):
    """Convert js/languages.xml to js/languages.json
    
    No argument. Outputs js/languages.json in the form
    {
      <msgid>: {
        <lang_1>: <msgstr_lang_1>,
        <lang_2>: <msgstr_lang_2>,
        ...
      },
      ...
    }
    """
    xml = ET.parse('js/languages.xml')
    xml_root = xml.getroot()
    translations = {}
    for xml_el in xml_root:
        translation = {}
        for xml_el_lang in xml_el:
            translation[xml_el_lang.tag] = xml_el_lang.text
        translations[xml_el.get('id')] = translation

    with open('js/languages.json', 'w') as json_file:
        json.dump(translations, json_file, ensure_ascii=False, indent=2)


def import_po(args):
    data = {}
    
    with open('js/languages.json', 'r') as json_file:
        data = json.load(json_file)
        
        directory = args.directory
        for translation in os.listdir(directory):
            lang = os.path.splitext(translation)[0]
            if lang == "ca@valencia":
                lang = "ca-valencia"
            elif lang == "pt_BR":
                lang = "pt-br"
            elif lang == "zh_CN":
                lang = "zh"

            dir_path = "locale/" + lang + "/LC_MESSAGES/"
            os.makedirs(dir_path, exist_ok=True)
            file_name = "announcement"
            po_file = dir_path + file_name
            shutil.copyfile(directory + "/" + translation, po_file + ".po")

            command = "msgfmt " + po_file + ".po -o " + po_file + ".mo"
            subprocess.run(command, shell=True, check=True)

            os.environ["LANGUAGE"] = lang
            gettext.bindtextdomain(file_name, os.path.abspath('locale'))
            gettext.textdomain(file_name)
            _ = gettext.gettext

            for value in data.values():
                english = value['en']
                translated = _(english)
                if english != translated:
                    value[lang] = translated
            print("Translations for " + lang + " imported")

    if data:
        with open('js/languages.json', 'w') as json_file:
            json.dump(data, json_file, ensure_ascii=False, indent=2)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='sub-command help')

    extract_cmd = subparsers.add_parser('extract', help='extract strings for translations')
    extract_cmd.add_argument('pot')
    extract_cmd.set_defaults(func=extract)
    
    xml2json_cmd = subparsers.add_parser('xml2json', help='convert xml to json')
    xml2json_cmd.set_defaults(func=xml2json)

    import_po_cmd = subparsers.add_parser('import', help='import translated strings')
    import_po_cmd.add_argument('directory')
    import_po_cmd.set_defaults(func=import_po)

    args = parser.parse_args()
    args.func(args)
